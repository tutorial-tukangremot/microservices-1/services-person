<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Person;

class PersonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            $person = Person::create([
                'id' => 1,
                'name' => 'Irvan Maulana',
                'school_id' => 1
            ]);
        } catch (\Throwable $th) {
            //
        }
    }
}
