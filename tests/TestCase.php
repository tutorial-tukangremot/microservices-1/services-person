<?php

namespace Tests;

use Laravel\Lumen\Testing\TestCase as BaseTestCase;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Faker\Factory as Faker;

abstract class TestCase extends BaseTestCase
{
    use DatabaseMigrations;

    public $faker;

    public function setUp(): void
    {
        parent::setUp();
        $this->faker = Faker::create();
    }

    public function createApplication()
    {
        return require __DIR__ . '/../bootstrap/app.php';
    }
}
