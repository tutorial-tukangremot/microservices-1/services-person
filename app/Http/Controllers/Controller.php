<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use App\Models\Person;
use App\Http\Resources\Person as PersonResource;

class Controller extends BaseController
{
    public function view(int $id)
    {
        $person = Person::findOrFail($id);

        return response()->json([
            'success' => true,
            'message' => 'success',
            'data' => new PersonResource($person)
        ], 200);
    }
}
