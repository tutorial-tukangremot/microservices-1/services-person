<?php

namespace Tests;

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;
use App\Models\Person;
use App\Services\School;

class RouteTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetPersonDetail()
    {
        // mocking services school
        Http::fake([
            config('services.school') . '/1' => Http::response(
                File::get(base_path('tests/files/school_detail.json')),
                200,
                []
            )
        ]);

        /**
         * start test with invalid data type
         */
        $this->get('/persons/a');

        $this->seeStatusCode(500);
        $this->seeJsonStructure([
            'success',
            'message',
        ]);
        /**
         * end test with invalid data type
         */

        /**
         * start test with invalid id
         */
        $this->get('/persons/1');

        $this->seeStatusCode(404);
        $this->seeJsonStructure([
            'success',
            'message',
        ]);
        /**
         * end test with invalid id
         */

        /**
         * start test with valid id
         */

        // create fake user
        $person = Person::factory()->create([
            'name' => $this->faker->name,
            'school_id' => 1
        ]);

        $this->get('/persons/' . $person->id);

        $this->seeStatusCode(200);
        $this->seeJson([
            'success' => true,
            'data' => [
                'id' => $person->id,
                'name' => $person->name,
                'school' => [
                    'id' => 1,
                    'name' => 'Sekolah Tutorial'
                ]
            ]
        ]);
        /**
         * end test with valid id
         */

        /**
         * start test with invalid/non-existing school_id in services school
         */

        // create fake user
        $person = Person::factory()->create([
            'name' => $this->faker->name,
            'school_id' => 1000
        ]);

        $this->get('/persons/' . $person->id);

        $this->seeStatusCode(200);
        $this->seeJson([
            'success' => true,
            'data' => [
                'id' => $person->id,
                'name' => $person->name,
                'school' => null
            ]
        ]);
        /**
         * end test with invalid/non-existing school_id in services school
         */
    }
}
