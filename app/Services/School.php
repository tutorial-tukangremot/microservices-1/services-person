<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;
use Illuminate\Http\Client\Response;

class School
{
    public function view(int $id): Response
    {
        return Http::get(config('services.school') . '/' . $id);
    }
}
