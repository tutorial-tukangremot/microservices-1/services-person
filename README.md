# Services Person
* [Request & Response Examples](#request--response-examples)

## Request & Response Examples
### API Resources

* Detail Person
    * [GET /persons/{id}](#get-persons--id--)

### GET /persons/{id}

Response body:

    {
        "success": true,
        "message": "success",
        "data": {
            "id": 1,
            "name": "Irvan Maulana",
            "school": {
                "id": 1,
                "name": "Sekolah Tutorial"
            }
        }
    }