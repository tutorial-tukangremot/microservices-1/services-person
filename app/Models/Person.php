<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Services\School;

class Person extends Model
{
    use HasFactory;

    protected $table = 'persons';


    protected $fillable = [
        'name', 'school_id'
    ];

    public function getSchoolAttribute()
    {
        if ($this->school_id) {
            try {
                $response = app(School::class)->view($this->school_id);
                $data = $response->json()['data'];

                return (object) [
                    'id' => $data['id'],
                    'name' => $data['name']
                ];
            } catch (\Throwable $th) {
                //
            }
        }

        return null;
    }
}
